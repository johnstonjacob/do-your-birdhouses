# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_NAME=./bin/do-your-birdhouses
BINARY_UNIX=./bin/$(BINARY_NAME)_unix

.PHONY: build
all: test build
build:
		$(GOBUILD) -o $(BINARY_NAME) -v ./cmd/do-your-birdhouses/main.go
test:
		$(GOTEST) -v ./...
clean:
		$(GOCLEAN) ./cmd/do-your-birdhouses/main.go
		rm -f $(BINARY_NAME)
		rm -f $(BINARY_UNIX)
run:
		$(GOBUILD) -o $(BINARY_NAME) -v ./cmd/do-your-birdhouses/main.go
		./$(BINARY_NAME)

# Cross compilation
build-linux:
		CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -o $(BINARY_UNIX) -v ./cmd/do-your-birdhouses/main.go
docker-build:
		docker run --rm -it -v "$(GOPATH)":/go -w /go/src/bitbucket.org/rsohlich/makepost golang:latest go build -o "$(BINARY_UNIX)" -v
