package main

import (
	cmd "gitlab.com/johnstonjacob/do-your-birdhouses/internal/cmd"
)

func main() {
	cmd.StartApp()
}
