// 141296528743464960
module.exports = {
  name: 'restart',
  description: 'start tracking your runs and recieve notifications',
  execute(message, args) {
  const {JAKE_USER_ID: id} = process.env
    if (message.author.id === id ) {
      message.channel.send('Restarting...').then(() => process.exit());
    }
  },
};
