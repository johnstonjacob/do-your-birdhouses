module.exports = {
  name: 'roll',
  description: 'rolls a number',
  execute(message, args) {
    const randomNumber = Math.floor(Math.random() * 100) + 1
    message.channel.send(randomNumber);
  },
};
