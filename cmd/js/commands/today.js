module.exports = {
  name: 'today',
  description: 'Checks how many runs you\'ve done today',
  execute(message, args) {
    message.channel.send('You\'ve done X birdhouse runs today.');
  },
};
