const fs = require('fs');

const Discord = require('discord.js');
const client = new Discord.Client();
const axios = require('axios');

const { DISCORD_CLIENT_TOKEN: token, DISCORD_PREFIX: prefix, SERVICE_NAME: service } = process.env;

client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter((file) => file.endsWith('.js'));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);

  client.commands.set(command.name, command);
}

client.once('ready', () => {
  console.log('Ready');
  client.user.setActivity('your birdhouses', { type: 'WATCHING' });
});

client.on('message', (message) => {
  if (!message.content.startsWith(prefix) || message.author.bot) return;

  const args = message.content.slice(prefix.length).split(' ');
  const commandName = args.shift().toLowerCase();

  if (!client.commands.has(commandName)) return;

  const command = client.commands.get(commandName);

  try {
    command.execute(message, args);
  } catch (error) {
    axios.post('http://localhost:8000/api/internal/sentry', {
      error,
      service,
    });
    message.reply('There was an error trying to execute that command!');
  }
});

client.login(token);
