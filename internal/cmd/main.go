package doyourbirdhouses

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/johnstonjacob/do-your-birdhouses/internal/monitoring"
	"go.uber.org/zap"
)

type server struct {
	db     string
	router *mux.Router
	email  string
	logger *zap.Logger
}

//StartApp begins the main do-your-birdhouses application, setting up and running the app.
func StartApp() error {
	logger, err := monitoring.Logger()

	if err != nil {
		logger.Fatal(err.Error())
	}

	logger.Info("do-your-birdhouses started", zap.Int("PID", os.Getpid()))

	monitoring.FlushSentry()

	s := newServer(logger)
	s.routes()

	logger.Fatal(http.ListenAndServe(":8000", s.router).Error())
	return nil
}

func newServer(logger *zap.Logger) *server {
	s := new(server)
	s.router = mux.NewRouter()
	s.logger = logger
	return s
}
