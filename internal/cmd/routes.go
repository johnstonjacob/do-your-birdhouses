package doyourbirdhouses

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/johnstonjacob/do-your-birdhouses/internal/monitoring"
)

func (s *server) routes() {
	s.router.HandleFunc("/api/user", s.handleUser())
	s.router.HandleFunc("/api/internal/sentry", s.handleSentry())
	//	s.router.HandleFunc("/about", s.handleAbout())
	//	s.router.HandleFunc("/", s.handleIndex())
}

func (s *server) handleUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.logger.Info("Request recieved to /api/user")
	}
}

func (s *server) handleSentry() http.HandlerFunc {
	type request struct {
		Error   string `json:"error"`
		Service string `json:"service"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		var req request

		s.logger.Info("Request recieved to /api/internal/sentry")

		err := unmarshalRequest(r, &req)

		if err != nil {

		}

		monitoring.ForwardSentry(req.Error, req.Service)
	}
}

func unmarshalRequest(req *http.Request, f interface{}) error {
	var bodyString string

	bodyBytes, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return errors.New("Error reading request body")
	}
	bodyString = string(bodyBytes)

	if err := json.Unmarshal([]byte(bodyString), &f); err != nil {
		return fmt.Errorf("Error unmarshalling JSON resposnse. Error: %s", err)
	}

	return nil
}
