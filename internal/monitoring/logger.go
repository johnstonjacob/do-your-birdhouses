package monitoring

import (
	"errors"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func createLogger() *zap.Logger {
	var logger *zap.Logger

	cfg := zap.Hooks(func(entry zapcore.Entry) error {
		if entry.Level == zapcore.ErrorLevel {
			sentry.CaptureException(errors.New(entry.Message))
		}

		if entry.Level >= zapcore.DPanicLevel {
			sentry.ConfigureScope(func(scope *sentry.Scope) {
				scope.SetLevel(sentry.LevelFatal)
			})
			sentry.CaptureException(errors.New(entry.Message))
			logger.Info("Flushing Sentry logs before exiting due to panic/fatal")
			FlushSentry()
		}

		return nil
	})

	logger, _ = zap.NewProduction(cfg)
	defer logger.Sync() // flushes buffer, if any
	return logger
}
