package monitoring

import (
	"go.uber.org/zap"
)

// Logger returns logging agent to be used throughout App. Will forward ErrorLevel+ logs to Sentry for debugging. Also configures the global Sentry hub, so sentry.CaptureException can be used if neccesary throughout the application. The more likely case is a logger event triggering the sentry message
func Logger() (*zap.Logger, error) {
	logger := createLogger()
	err := createSentry(logger)

	if err != nil {
		return logger, err
	}

	return logger, nil
}
