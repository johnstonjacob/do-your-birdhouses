package monitoring

import (
	"errors"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
	"go.uber.org/zap"
)

func createSentry(logger *zap.Logger) error {
	dsn := os.Getenv("SENTRY_DSN")
	err := sentry.Init(sentry.ClientOptions{
		Dsn:   dsn,
		Debug: false,
		BeforeSend: func(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
			logger.Info("Sending error ID %s", zap.String("event-id", string(event.EventID)))
			return event
		},
	})

	if err != nil {
		return errors.New("SENTRY_DSN not set")
	}

	return nil
}

//ForwardSentry sends an exception to sentry with a given string
func ForwardSentry(s, service string) {
	sentry.WithScope(func(scope *sentry.Scope) {
		scope.SetTag("service", service)
		sentry.CaptureException(errors.New(s))
	})
}

//FlushSentry gives the sentry alert queue time to flush
func FlushSentry() {
	sentry.Flush(time.Second * 2)
}
